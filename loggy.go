package loggy

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/brendan1538/stringy"
)

type colorcodes struct {
	reset, red, green, yellow string
}

var colorscheme = map[string]string{
	"reset":   "\033[0m",
	"error":   "\033[31m",
	"fatal":   "\033[31m",
	"info":    "\033[32m",
	"warning": "\033[33m",
}

func formatLevel(level string) string {
	trailingColor := "reset"
	if level == "fatal" {
		trailingColor = "fatal"
	}

	outputLevel := stringy.Trunc(level, 3)
	formattedLevel := fmt.Sprintf("%v[%v]%v", colorscheme[level], strings.ToUpper(outputLevel), colorscheme[trailingColor])

	return formattedLevel
}

// HandleError is a function that will check for an error's existence. if there is an error, it will log it
// as a red [FATAL] line to stdout and terminate execution. The entire error will be in red to differentiate
// from a non-fatal Error log
// @arg err (interface{})
// @arg msg (string) - optional
func HandleError(err interface{}) {
	if err != nil {
		log.Fatalf("%v %v %v\n", formatLevel("fatal"), err, colorscheme["reset"])
	}
}

// Info will log to stdout. will appear as a green [INFO] line
// @arg msg (interface{})
func Info(msg interface{}) {
	log.Printf("%v %v\n", formatLevel("info"), msg)
}

// Warning will log to stdout. will appear as an orange [WARN] line
// @arg msg (interface{})
func Warning(msg interface{}) {
	log.Printf("%v %v\n", formatLevel("warning"), msg)
}

// Error will log to stdout. will appear as a red [ERRO] line
// @arg msg (interface{})
func Error(msg interface{}) {
	log.Printf("%v %v\n", formatLevel("error"), msg)
}
